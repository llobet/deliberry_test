export default class product {
    constructor (name) {
        this.name = 'Producte '+name;
        this.brand = 'Marca';
        this.price = this.getPrice();
    }


    getPrice() {
		return Math.round( (Math.random() * (100 - 50) + 100) * 100 )/100;
	}
}
