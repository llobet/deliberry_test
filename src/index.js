'use strict';


import logo from './assets/images/logo.svg';
import './assets/styles/styles.scss';

import './assets/fonts/_fonts.scss';

import Product from './assets/js/product';
import $ from 'jquery';

import html from './templates/index.html';

$('.nav__logo').html(`<h1><img src="${logo}" alt="deliberry"></h1>`);

let menu__item = $('.menu__item');
let main__content = $(".main__content");

// MENU

let bind = (el, content, easing) => {
	let tab = el.find('section#'+content);
	let others = el.find('section').not('#'+content);

	others
	.fadeOut(easing, function(){

		tab.fadeIn(easing);

	})
};



menu__item
.on( 'click', function(){
	let el = $(this);
	let section = el.attr('data-href');

	menu__item.removeClass('active');
	el.addClass('active');

	bind(main__content, section, 300);
});



//Products

for (var i = 1; i <= 14; i++) {
	let product = new Product(i);

	
	$(`<div id="product${i}" class="product" />`)
	.append(`
		<img alt='${product.name}'>
		<p class="name">${product.name}</p>
		<p class="brand">${product.brand}</p>
		<p class="price">${product.price}</p>
	`)
	.appendTo(
		$(main__content)
		.find('#productos')
		.find('.main__productos')
	);
};



//Categories (different products)

for (var n = 15; i <= 28; i++) {
	let product = new Product(i);

	
	$(`<div id="product${i}" class="product" />`)
	.append(`
		<img alt='${product.name}'>
		<p class="name">${product.name}</p>
		<p class="brand">${product.brand}</p>
		<p class="price">${product.price}</p>
	`)
	.appendTo(
		$(main__content)
		.find('#categorias')
		.find('.main__productos')
	);
};





console.log($(window));


